﻿using System;
using Microsoft.Win32;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kunimi.Models;
using Prism.Mvvm;
using Prism.Commands;
using System.Reactive.Linq;
using System.ComponentModel;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;
using Kunimi.Common;

namespace Kunimi.ViewModels
{
    class FormatterViewModel : BindableBase, INotifyDataErrorInfo
    {
        private FormatterModel formatter;
        public FormatterModel Formatter
        {
            get { return this.formatter; }
            set { SetProperty(ref this.formatter, value); }
        }

        private string startingChar;
        public string StartingChar
        {
            get { return this.startingChar; }
            set { SetProperty(ref this.startingChar, value); }
        }

        private ObservableCollection<EdgeCharPair> edgeCharMap;
        public ObservableCollection<EdgeCharPair> EdgeCharMap
        {
            get { return this.edgeCharMap; }
            set { SetProperty(ref this.edgeCharMap, value); }
        }

        private string endingChar;
        public string EndingChar
        {
            get { return this.endingChar; }
            set { SetProperty(ref this.endingChar, value); }
        }

        public FormatterViewModel()
        {
            this.Formatter = new FormatterModel();
            this.EdgeCharMap = new ObservableCollection<EdgeCharPair>();
        }

        private string inputFilePath;
        public string InputFilePath
        {
            get { return this.inputFilePath; }
            set { SetProperty(ref this.inputFilePath, value); }
        }

        private string outputFilePath;
        public string OutputFilePath
        {
            get { return this.outputFilePath; }
            set { SetProperty(ref this.outputFilePath, value); }
        }

        private DelegateCommand formatCommand;
        public DelegateCommand FormatCommand
        {
            get { return this.formatCommand = this.formatCommand ?? new DelegateCommand(Format); }
        }

        private DelegateCommand extractCommand;
        public DelegateCommand ExtractCommand
        {
            get { return this.extractCommand = this.extractCommand ?? new DelegateCommand(ExtractPrettyTextFile); }
        }

        private void Format()
        {
            this.Formatter.GeneratePrettyText(this.EdgeCharMap.ToList(), InputFilePath);
            if (Formatter.NoNameSerifLineNumber > 0)
            {
                this.ErrorMessenger.Raise(
                    new Message($"{Formatter.NoNameSerifLineNumber}行目に名前の割当てがありません"),
                    m => { }
                    );
            }
        }

        private void ExtractPrettyTextFile()
        {
            this.Formatter.GeneratePrettyTextFile(OutputFilePath);
        }

        private DelegateCommand addPairCommand;
        public DelegateCommand AddPairCommand
        {
            get
            {
                return this.addPairCommand =
                  this.addPairCommand ?? new DelegateCommand(AddPair, CanExecuteAddPairCommand)
                  .ObservesProperty(() => StartingChar)
                  .ObservesProperty(() => EndingChar);
            }
        }

        private bool CanExecuteAddPairCommand()
        {
            return !string.IsNullOrWhiteSpace(this.StartingChar)
                && !string.IsNullOrWhiteSpace(this.EndingChar);
        }

        private void AddPair()
        {
            var newEdgePair = new EdgeCharPair(this.StartingChar, this.EndingChar);
            this.EdgeCharMap.Add(newEdgePair);

            // 入力ボックスの初期化
            this.StartingChar = String.Empty;
            this.EndingChar = String.Empty;
        }

        private DelegateCommand removePairCommand;
        public DelegateCommand RemovePairCommand
        {
            get
            {
                return this.removePairCommand =
                    this.removePairCommand ?? new DelegateCommand(RemovePair, CanExecuteRemovePairCommand)
                        .ObservesProperty(() => SelectedPair);
            }
        }

        private EdgeCharPair selectedPair;
        public EdgeCharPair SelectedPair
        {
            get { return this.selectedPair; }
            set { SetProperty(ref this.selectedPair, value); }
        }

        private bool CanExecuteRemovePairCommand()
        {
            return this.EdgeCharMap.Count != 0;
        }

        private void RemovePair()
        {
            this.EdgeCharMap.Remove(this.SelectedPair);
        }



        private Dictionary<string, IEnumerable> errors = new Dictionary<string, IEnumerable>();
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
        private void OnErrorsChanged([CallerMemberName] string propertyName = null)
        {
            this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
        }

        public IEnumerable GetErrors(string propertyName)
        {
            IEnumerable error = null;
            this.errors.TryGetValue(propertyName, out error);
            return error;
        }

        public bool HasErrors
        {
            get { return this.errors.Values.Any(e => e != null); }
        }

        private Messenger errorMessenger = new Messenger();
        public Messenger ErrorMessenger
        {
            get { return this.errorMessenger; }
        }
    }
}
