﻿using System;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Mvvm;
using System.Collections.ObjectModel;

namespace Kunimi.Models
{
    class FormatterModel : BindableBase
    {
        public FormatterModel()
        {
        }

        private int noNameSerifLineNumber;
        public int NoNameSerifLineNumber
        {
            get { return this.noNameSerifLineNumber; }
            set { SetProperty(ref this.noNameSerifLineNumber, value); }
        }

        private string desiredText;
        public string DesiredText
        {
            get { return this.desiredText; }
            set { SetProperty(ref this.desiredText, value); }
        }

        private int serifCount;
        public int SerifCount
        {
            get { return this.serifCount; }
            set { SetProperty(ref this.serifCount, value); }
        }

        private Dictionary<string, int> serifCountPerCharacter;
        public Dictionary<string, int> SerifCountPerCharacter
        {
            get { return this.serifCountPerCharacter; }
            set { SetProperty(ref this.serifCountPerCharacter, value); }
        }

        public void GeneratePrettyText(List<EdgeCharPair> EdgeCharMap, string InputFilePath)
        {
            string[] textArray = File.ReadAllLines(InputFilePath);

            string text = String.Join(Environment.NewLine, textArray);

            foreach (EdgeCharPair entry in EdgeCharMap)
            {
                if (entry.IsErasable)
                {
                    text = Regex.Replace(
                            text,
                            $@"\{entry.StartingChar}.*?\{entry.EndingChar}",
                            String.Empty,
                            RegexOptions.Singleline
                    );
                }
            }

            this.NoNameSerifLineNumber = 0;
            this.SerifCount = 0;
            this.SerifCountPerCharacter = new Dictionary<string, int>();

            Regex serifSeparator = new Regex(@"(?<Name>[\p{IsHiragana}\p{IsKatakana}\p{IsCJKUnifiedIdeographs}\d]*?|^$)(?<Serif>「.*?」)", RegexOptions.Singleline);

            Match part = serifSeparator.Match(text);

            while (part.Success)
            {
                if (String.IsNullOrWhiteSpace(part.Groups["Name"].Value))
                {
                    //Splitした値は最後に空白が入るためTrimする.
                    //0スタートなので1プラス
                    this.NoNameSerifLineNumber = Array.IndexOf(textArray, part.Groups["Serif"].Value.Split('\n')[0].Trim()) + 1;
                    return;
                }

                this.SerifCount += 1;
                var zenkakuCount = Strings.StrConv(String.Format("{0:D3}", this.SerifCount), VbStrConv.Wide);
                var prettySerif = part.Groups["Serif"].Value.Replace("\n", "\n\t\t\t  ");

                string characterName = part.Groups["Name"].Value.Trim();

                text = Regex.Replace(
                        text,
                        $"{characterName}{part.Groups["Serif"].Value}",
                        $"{characterName}\t\t{zenkakuCount}{prettySerif}"
                );

                if (this.SerifCountPerCharacter.ContainsKey(characterName))
                {
                    this.SerifCountPerCharacter[characterName] += 1;
                }
                else
                {
                    this.SerifCountPerCharacter[characterName] = 1;
                }

                part = part.NextMatch();
            }

            text = text.Replace("　", " ");
            text += "\n-----キャラ別セリフ数-----\n";

            foreach (KeyValuePair<string, int> pair in this.SerifCountPerCharacter)
            {
                text += String.Format("{0}: {1}個\n", pair.Key, pair.Value);
            }

            this.DesiredText = text;
            
        }

        public void GeneratePrettyTextFile(string OutputFilePath)
        {
            File.WriteAllText(OutputFilePath, desiredText);
        }

    }

    class EdgeCharPair : BindableBase
    {
        private bool isErasable;
        public bool IsErasable
        {
            get { return this.isErasable; }
            set { SetProperty(ref this.isErasable, value); }
        }

        private string startingChar;
        public string StartingChar
        {
            get { return this.startingChar; }
            set { SetProperty(ref this.startingChar, value); }
        }

        private string endingChar;
        public string EndingChar
        {
            get { return this.endingChar; }
            set { SetProperty(ref this.endingChar, value); }
        }

        public EdgeCharPair(string startingChar, string endingChar)
        {
            this.IsErasable = true;
            this.StartingChar = startingChar;
            this.EndingChar = endingChar;
        }
    }
}
